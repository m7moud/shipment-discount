package command_test

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Command Suite")
}

func captureOutputLines(f func()) []string {
	rescueStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w
	f()
	w.Close()

	out, _ := ioutil.ReadAll(r)
	os.Stdout = rescueStdout

	return strings.Split(strings.TrimSuffix(string(out), "\n"), "\n")
}
