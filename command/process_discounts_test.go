package command_test

import (
	"gitlab.com/m7moud/shipment-discount/command"
	"gitlab.com/m7moud/shipment-discount/helper"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("TransactionsDiscountProcessorCommand", func() {
	When("input file give", func() {
		It("print expected output", func() {
			c, _ := command.NewTransactionsDiscountProcessorCommand("../fixture/input.txt")
			output := captureOutputLines(func() {
				c.Execute()
			})

			expected, _ := helper.ReadLines("../fixture/output.txt")
			Expect(output).To(Equal(expected))
		})
	})
})
