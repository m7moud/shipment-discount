package command

import (
	"fmt"

	"gitlab.com/m7moud/shipment-discount/helper"
	"gitlab.com/m7moud/shipment-discount/model"
	"gitlab.com/m7moud/shipment-discount/registry"
	"gitlab.com/m7moud/shipment-discount/strategy"
)

// TransactionsDiscountProcessorCommand TODO
type TransactionsDiscountProcessorCommand struct {
	filePath string
	// Kept the array of file lines, to maintine the lines order
	fileLines []string
	registry  *registry.TransactionRegistry
}

// NewTransactionsDiscountProcessorCommand  represents a discount processor command
func NewTransactionsDiscountProcessorCommand(filePath string) (*TransactionsDiscountProcessorCommand, error) {
	return &TransactionsDiscountProcessorCommand{
		filePath: filePath,
		registry: registry.NewTransactionRegistry(),
	}, nil
}

// Execute executes the command, returing error is any
func (c *TransactionsDiscountProcessorCommand) Execute() error {
	var err error
	c.fileLines, err = helper.ReadLines(c.filePath)
	if err != nil {
		return err
	}

	c.registerTransactions()
	c.applyDiscounts()
	c.printResult()

	return nil
}

func (c *TransactionsDiscountProcessorCommand) registerTransactions() {
	for i, line := range c.fileLines {
		t, err := model.NewTransaction(line)
		if err == nil {
			c.registry.Register(t, i)
		}
	}
}

func (c *TransactionsDiscountProcessorCommand) applyDiscounts() {
	for i := range c.fileLines {
		t, err := c.registry.Get(i)
		if err != nil {
			continue
		}

		applyDiscounts(c.registry, t)
		c.registry.MarkProcessed(i)
	}
}

func (c *TransactionsDiscountProcessorCommand) printResult() {
	for i, line := range c.fileLines {
		if t, ok := c.registry.Processed[i]; ok {
			fmt.Printf("%+v\n", t)
			continue
		}

		fmt.Printf("%s Ignored\n", line)
	}
}

func applyDiscounts(r *registry.TransactionRegistry, t *model.Transaction) {
	input := &strategy.DiscountStrategyInput{
		Registry:    r,
		Transaction: t,
	}

	rules := []strategy.DiscountStrategy{
		strategy.SmallShipmentsShouldMatchLowestSmallPackagePrice{DiscountStrategyInput: input},
		strategy.ThirdLargeLPShipmentFreeOnceMonth{DiscountStrategyInput: input},
		// Is expected to always be at the end
		&strategy.Max10EURMonthly{DiscountStrategyInput: input},
	}

	for _, rule := range rules {
		if rule.Applicable() {
			rule.Apply()
		}
	}
}
