package command

// Command encapsulate all methods needed to execute an action
type Command interface {
	Execute() error
}
