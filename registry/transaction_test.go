package registry

import (
	reflect "reflect"
	"testing"

	"gitlab.com/m7moud/shipment-discount/model"
)

func TestTransactionRegistry_Get(t *testing.T) {
	type fields struct {
		registered map[int]*model.Transaction
		Processed  map[int]*model.Transaction
	}

	type args struct {
		ID int
	}

	validTransaction, _ := model.NewTransaction("2015-03-01 S MR")

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Transaction
		wantErr bool
	}{
		{
			name: "Transaction does not exist",
			fields: fields{
				registered: make(map[int]*model.Transaction),
			},
			args: args{
				ID: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Transaction exists",
			fields: fields{
				registered: map[int]*model.Transaction{
					1: validTransaction,
				},
			},
			args: args{
				ID: 1,
			},
			want:    validTransaction,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &TransactionRegistry{
				registered: tt.fields.registered,
				Processed:  tt.fields.Processed,
			}

			got, err := r.Get(tt.args.ID)

			if (err != nil) != tt.wantErr {
				t.Errorf("TransactionRegistry.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TransactionRegistry.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
