package registry

import (
	"errors"

	"gitlab.com/m7moud/shipment-discount/model"
)

// TransactionRegistry tracks all transactions and their IDs
// processed transactions are expected to be deleted
type TransactionRegistry struct {
	registered map[int]*model.Transaction
	Processed  map[int]*model.Transaction
}

// NewTransactionRegistry initiates a new TransactionRegistry
func NewTransactionRegistry() *TransactionRegistry {
	return &TransactionRegistry{
		registered: make(map[int]*model.Transaction),
		Processed:  make(map[int]*model.Transaction),
	}
}

// Register adds a transaction to the registry
func (r *TransactionRegistry) Register(t *model.Transaction, ID int) {
	r.registered[ID] = t
}

// Get returns a transaction by ID
func (r *TransactionRegistry) Get(ID int) (*model.Transaction, error) {
	if _, ok := r.registered[ID]; !ok {
		return nil, errors.New("Transaction is not registered")
	}

	return r.registered[ID], nil
}

// MarkProcessed Adds a processed transaction to the processed transaction registry
// Removes transaction from the registered transaction registry
func (r *TransactionRegistry) MarkProcessed(ID int) error {
	_, err := r.Get(ID)
	if err != nil {
		return err
	}

	r.Processed[ID] = r.registered[ID]
	delete(r.registered, ID)

	return nil
}

// SumCalenderMonthDiscount returns sum of discounts for specified calender month
func (r *TransactionRegistry) SumCalenderMonthDiscount(m *model.CalenderMonth) float64 {
	var sum float64
	for _, t := range r.Processed {
		if m.Same(t.Date) {
			sum += t.TotalDiscount()
		}
	}

	return sum
}

// CountCarrierCalenderMonthDiscount returns count of discounts for specified carrier, size, and calender month
func (r *TransactionRegistry) CountCarrierCalenderMonthDiscount(c model.Carrier, s model.PackageSizeCode, m *model.CalenderMonth) int {
	count := 0

	for _, t := range r.Processed {
		if t.Carrier.Code == c.Code && t.PackageSizeCode == s && m.Same(t.Date) {
			count++
		}
	}

	return count
}
