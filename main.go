package main

import (
	"flag"

	"gitlab.com/m7moud/shipment-discount/command"
	"gitlab.com/m7moud/shipment-discount/helper"
)

func main() {
	flag.Parse()
	filePath := flag.Arg(0)
	if filePath == "" {
		filePath = "input.txt"
	}

	transactionsDiscountCommand, _ := command.NewTransactionsDiscountProcessorCommand(filePath)
	err := transactionsDiscountCommand.Execute()
	helper.ExitGracefullyIfError(err)
}
