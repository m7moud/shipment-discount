# Shipment Discount Calculation

- I'm new to Golang but I have used it to practice implementing the same concepts in a different language.
- Only the language library is used, no additional libraries except for testing.
- I have mixed the style of the tests, some tests are BDD and some are not.
- Test coverage is not 100% but there's a test case for the input and output from the problem description.
- I found the problem to be confusing, it can not scale and it can't be extended, because of the command line input requirement for example. I was worried that I would over-engineer it, but I hope it's simple enough.
- The strategy design pattern allows it to be open for extension, you also need to keep a registry to allow for history search and aggregation.

## Run

Run `docker-compose run shipment-discount bin/apply_discounts ./fixture/input.txt`

## Tests

Run `docker-compose run shipment-discount go test ./... -v` or you can also check the repository pipeline
