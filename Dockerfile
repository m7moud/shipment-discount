FROM golang:1.16.4

WORKDIR /go/src/gitlab.com/m7moud/shipment-discount/

RUN apt-get update && \
    apt-get install -y build-essential

COPY . .

RUN go get -d -v ./...

RUN go install -v ./...

RUN go build -o ./bin/apply_discounts .

