package model

type discountType int

const (
	// Shipment is discount that applies to shipment prices
	Shipment discountType = iota
)

// Discount represents a reduction in a certain amount
type Discount struct {
	Type   discountType
	Amount float64
}

// TotalDiscount returns the total amount of the discounts for transaction
func (t *Transaction) TotalDiscount() float64 {
	var sum float64
	for _, d := range t.Discounts {
		sum += d.Amount
	}

	return sum
}

// TotalDiscountForType returns the total discount amount for a certain discount type
func (t *Transaction) TotalDiscountForType(dType discountType) float64 {
	var sum float64
	for _, d := range t.Discounts {
		if d.Type != dType {
			continue
		}
		sum += d.Amount
	}

	return sum
}
