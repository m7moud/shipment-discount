package model

import (
	"fmt"
	"strings"
	"time"
)

// Transaction represents the basic transaction
type Transaction struct {
	Date            time.Time
	PackageSizeCode PackageSizeCode
	Carrier         Carrier
	Discounts       []*Discount
}

// NewTransaction init new transaction
func NewTransaction(input string) (*Transaction, error) {
	data := strings.Split(input, " ")

	date, err := time.Parse("2006-01-02", data[0])
	if err != nil {
		return nil, err
	}

	return &Transaction{
		Date:            date,
		PackageSizeCode: PackageSizeCode(data[1]),
		Carrier:         CarrierRegistry[data[2]],
	}, nil
}

func (t Transaction) String() string {
	actualShipmentPrice := t.ShipmentPrice() - t.TotalDiscount()
	formattedDiscount := fmt.Sprintf("%.2f", t.TotalDiscount())
	if t.TotalDiscount() == 0 {
		formattedDiscount = "-"
	}

	return fmt.Sprintf(
		"%s %s %s %.2f %s",
		t.Date.Format("2006-01-02"),
		t.PackageSizeCode,
		t.Carrier.Code,
		actualShipmentPrice,
		formattedDiscount,
	)
}
