package model

// PackageSizeCode represents the size of shipment package
type PackageSizeCode string

const (
	// S Small, a popular option to ship jewelry
	S PackageSizeCode = "S"
	// M Medium - clothes and similar items
	M PackageSizeCode = "M"
	// L Large - mostly shoes
	L PackageSizeCode = "L"
)
