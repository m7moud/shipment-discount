package model

import (
	"fmt"
	"time"
)

// CalenderMonth represents a calendar month, with year and month only
type CalenderMonth struct {
	Month time.Month
	Year  int
}

// NewCalenderMonth creates a new calendar month
func NewCalenderMonth(time time.Time) *CalenderMonth {
	return &CalenderMonth{
		Month: time.Month(),
		Year:  time.Year(),
	}
}

func (m *CalenderMonth) String() string {
	return fmt.Sprintf("%02d-%04d", m.Month, m.Year)
}

// Same returns true if the calendar month is same as for given time
func (m *CalenderMonth) Same(time time.Time) bool {
	return m.Month == time.Month() && m.Year == time.Year()
}
