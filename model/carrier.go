package model

import (
	"errors"
	"math"
)

// Carrier represents a shipment provider
type Carrier struct {
	Name   string
	Code   string
	prices map[PackageSizeCode]float64
}

// CarrierRegistry is a list of available carriers
var CarrierRegistry = map[string]Carrier{
	"LP": {
		Code: "LP",
		Name: "La Poste",
		prices: map[PackageSizeCode]float64{
			"S": 1.5,
			"M": 4.9,
			"L": 6.9,
		},
	},
	"MR": {
		Code: "MR",
		Name: "Mondial Relay",
		prices: map[PackageSizeCode]float64{
			"S": 2.0,
			"M": 3.0,
			"L": 4.0,
		},
	},
}

func (c *Carrier) String() string {
	return c.Code
}

// ShipmentPrice return price for a given size
func (c *Carrier) ShipmentPrice(size PackageSizeCode) float64 {
	return c.prices[size]
}

// LowestSizeShipmentPrice return lowest price for a given size
func (c *Carrier) LowestSizeShipmentPrice(size PackageSizeCode) float64 {
	min := math.Inf(1)
	for _, carrier := range CarrierRegistry {
		min = math.Min(carrier.ShipmentPrice(size), min)
	}

	return min
}

// ShipmentPrice return shipment price for a given transaction
func (t Transaction) ShipmentPrice() float64 {
	return CarrierRegistry[t.Carrier.Code].prices[t.PackageSizeCode]
}

// AddShipmentDiscount Adds a discount of type `Shipment`
// returns an error if there's already a shipment discount
func (t *Transaction) AddShipmentDiscount(amount float64) error {
	for _, d := range t.Discounts {
		if d.Type == Shipment {
			return errors.New("Only one shipment discount is allowed")
		}
	}

	t.Discounts = append(t.Discounts, &Discount{
		Type:   Shipment,
		Amount: amount,
	})

	return nil
}

// UpdateShipmentDiscount Updates the existing discount of type `Shipment`
// returns an error if no shipment discount is found
func (t *Transaction) UpdateShipmentDiscount(amount float64) error {
	for _, d := range t.Discounts {
		if d.Type != Shipment {
			continue
		}

		d.Amount = amount

		return nil
	}

	return errors.New("No shipment discount to update")
}
