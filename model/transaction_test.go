package model_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/m7moud/shipment-discount/model"
)

var _ = Describe("NewTransaction", func() {
	When("valid", func() {
		It("returns Transaction", func() {
			t, err := model.NewTransaction("2015-03-01 S MR")

			Expect(err).NotTo(HaveOccurred())
			Expect(string(t.PackageSizeCode)).To(Equal("S"))
			Expect(t.Carrier.String()).To(Equal("MR"))
		})
	})

	When("invalid", func() {
		examples := []string{
			"2015-02-29 S SR",
			"2015-02-29 X MR",
			"2015-02-29 S MR",
			"2015-02-29 CUSPS",
		}

		for _, example := range examples {
			It("returns an error", func() {
				_, err := model.NewTransaction(example)
				Expect(err).To(HaveOccurred())
			})
		}
	})
})
