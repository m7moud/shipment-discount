package strategy

import "gitlab.com/m7moud/shipment-discount/model"

// ThirdLargeLPShipmentFreeOnceMonth represents a discount strategy
// Third L shipment via LP should be free, but only once a calendar month
type ThirdLargeLPShipmentFreeOnceMonth struct {
	*DiscountStrategyInput
}

// Applicable return true if strategy should be applied
func (s ThirdLargeLPShipmentFreeOnceMonth) Applicable() bool {
	return s.Transaction.Carrier.Code == "LP" &&
		string(s.Transaction.PackageSizeCode) == "L" &&
		s.Registry.CountCarrierCalenderMonthDiscount(
			s.Transaction.Carrier,
			s.Transaction.PackageSizeCode,
			model.NewCalenderMonth(s.Transaction.Date),
		) == 2
}

// Apply creates and registers discounted transaction
func (s ThirdLargeLPShipmentFreeOnceMonth) Apply() {
	s.Transaction.AddShipmentDiscount(s.Transaction.ShipmentPrice())
}
