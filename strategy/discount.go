package strategy

import (
	"gitlab.com/m7moud/shipment-discount/model"
	"gitlab.com/m7moud/shipment-discount/registry"
)

// DiscountStrategy is a strategy that for applying a discount
type DiscountStrategy interface {
	Applicable() bool
	Apply()
}

// DiscountStrategyInput encapsulates a transaction to be disounted
type DiscountStrategyInput struct {
	Registry    *registry.TransactionRegistry
	Transaction *model.Transaction
}
