package strategy

import (
	"gitlab.com/m7moud/shipment-discount/model"
)

// SmallShipmentsShouldMatchLowestSmallPackagePrice represents a discount strategy
// All S shipments should always match the lowest S package price among the providers
type SmallShipmentsShouldMatchLowestSmallPackagePrice struct {
	*DiscountStrategyInput
}

// Applicable return true if strategy should be applied
func (s SmallShipmentsShouldMatchLowestSmallPackagePrice) Applicable() bool {
	return s.Transaction.PackageSizeCode == model.S &&
		s.Transaction.Carrier.ShipmentPrice(s.Transaction.PackageSizeCode) > s.Transaction.Carrier.LowestSizeShipmentPrice(s.Transaction.PackageSizeCode)
}

// Apply creates and registers discounted transaction
func (s SmallShipmentsShouldMatchLowestSmallPackagePrice) Apply() {
	ShipmentPrice := s.Transaction.Carrier.LowestSizeShipmentPrice(s.Transaction.PackageSizeCode)
	s.Transaction.AddShipmentDiscount(s.Transaction.ShipmentPrice() - ShipmentPrice)
}
