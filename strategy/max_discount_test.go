package strategy

import (
	"testing"
	"time"

	"gitlab.com/m7moud/shipment-discount/model"
	"gitlab.com/m7moud/shipment-discount/registry"
)

func TestMax10EURMonthly_Apply(t *testing.T) {
	type fields struct {
		DiscountStrategyInput *DiscountStrategyInput
	}

	current := &model.Transaction{
		Date:            time.Now(),
		PackageSizeCode: model.S,
		Carrier:         model.CarrierRegistry["MR"],
		Discounts: []*model.Discount{
			{
				Type:   model.Shipment,
				Amount: 1.1,
			},
		},
	}

	registry := registry.NewTransactionRegistry()
	registry.Register(current, 1)
	registry.Processed = map[int]*model.Transaction{
		2: {
			Date:            time.Now(),
			PackageSizeCode: model.S,
			Carrier:         model.CarrierRegistry["MR"],
			Discounts: []*model.Discount{
				{
					Type:   model.Shipment,
					Amount: 9.9,
				},
			},
		},
	}

	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Update is required",
			fields: fields{
				DiscountStrategyInput: &DiscountStrategyInput{
					Transaction: current,
					Registry:    registry,
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Max10EURMonthly{
				DiscountStrategyInput: tt.fields.DiscountStrategyInput,
			}
			s.Apply()
			s.Registry.MarkProcessed(1)

			discount := s.Registry.SumCalenderMonthDiscount(model.NewCalenderMonth(time.Now()))

			if discount > 10 {
				t.Errorf("TestMax10EURMonthly.Apply() discount = %.2f > 10.00", discount)
			}
		})
	}
}
