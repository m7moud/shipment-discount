package strategy

import (
	"gitlab.com/m7moud/shipment-discount/model"
)

// Max10EURMonthly represents a discount strategy
// Accumulated discounts cannot exceed 10 € in a calendar month
// If there are not enough funds to fully cover a discount this calendar month, it should be covered partially
type Max10EURMonthly struct {
	*DiscountStrategyInput
}

// Applicable return true if strategy should be applied
func (s Max10EURMonthly) Applicable() bool {
	return true
}

// Apply creates and registers discounted transaction
func (s Max10EURMonthly) Apply() {
	sumDiscount := s.Registry.SumCalenderMonthDiscount(model.NewCalenderMonth(s.Transaction.Date))
	transactionDiscount := s.Transaction.TotalDiscountForType(model.Shipment)
	if sumDiscount+transactionDiscount > 10 {
		transactionDiscount = float64(1000-int(sumDiscount*100)) / 100

		s.Transaction.UpdateShipmentDiscount(transactionDiscount)
	}
}
