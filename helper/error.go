package helper

import (
	"fmt"
	"os"
)

// ExitGracefullyIfError exits the program gracefully with the appropriate status
func ExitGracefullyIfError(err error) {
	if err == nil {
		return
	}

	fmt.Fprintf(os.Stderr, "error: %v\n", err)
	os.Exit(1)
}
